============================================================
Short Documentation of faketoys
============================================================
.. highlight:: python
   :linenothreshold: 5

Introduction
============================================================
Statistical distributions can be unintuitive and surprising at times. It can often be helpful to simulate configurations of statistical processes with random variables. This package was first created to generate random variables to simulate the the Fake Factor Method used that is commonly known in High Energy Physics. Despite this specific goal in mind, a lot of the underlying methods are general enough to easily use the package to generate random variables from other distributions and to interpret them.

Dependencies
============================================================
To explore how a distribution behaves if an input variable changes, it can be useful to create multiple distributions with different input variables and compare them. To generalize this and allow such comparisons in multiple dimensions, the `xarray` package is used. The interface with `dask` allows to parallelize the computing for these different distributions.

The random number generators and mathematical functions are taken from `numpy`. Statistics functions also come from `scipy`. The results can easily be visualized through `xarray` or directly with `matplotlib`. The `uncertainties` package is included for Gaussian error propagation, even though this is not used extensively at this point.

The package `coverage` is used to keep track of code testing. The documentation is built with `Sphinx`.

.. _drawing-random-numbers:

Drawing random numbers
============================================================
Here an example to draw random numbers from a Gaussian distribution:

.. doctest:: rand

  >>> from faketoys.stats.dists import Gaussian
  >>> from faketoys.stats.dist_base import DistributionBase
  >>> _ = DistributionBase.setRNG(seed=10)
  >>> gaus_mean = 10
  >>> stddev = 1
  >>> g = Gaussian(gaus_mean, stddev)
  >>> rand_var = g.getRandom()
  >>> print(rand_var)
  8.896661550934468

The method :py:meth:`DistributionBase.setRNG() <faketoys.stats.dist_base.DistributionBase.setRNG()>` (line 3) is called to initialize the random number generator. This allows to get reproducible results. In line 6, we create an instance of a Gaussian distribution with given mean and standard deviation and draw a random number from it in the following line.

If you want to combine different distributions, you can use classes that derive from :py:class:`~faketoys.stats.derived_dist_base.DerivedDistributionBase`. Here an example, where the sum of a Gaussian and Poisson variable is calculated (extending the snippet from above):

.. doctest:: rand

   >>> from faketoys.stats.dists import Poisson
   >>> from faketoys.stats.derived_dists import SumOfDistributions
   >>> poisson_mean = 100
   >>> p = Poisson(poisson_mean)
   >>> s = SumOfDistributions([p, g], name="Sum")
   >>> rand_sum = s.getRandom()
   >>> print(rand_sum)
   101.2669758563944

If you want to get a list of the true values of the distributions varied by their uncertainties, you can call:

.. doctest:: rand

   >>> s.getDictOfVariedValues()
   {Poisson(100.0): [120.0, 100.0], Gaussian(10.0, 1.0): [111.0, 109.0]}


Pseudo experiments in two steps
============================================================
You can initialize distributions yourself and draw random numbers from them as shown above. When you want to explore many distributions at the same time (say a 2-dimensional grid of distributions), it makes sense to store them in an array to have easy access. `faketoys` works well together with `xarray`. I would recommend a two-step procedure to generate random variables.

1. In a first step, you define the 2-dimensional grid with `xarray.DataArray`, assign distributions to the entries of the grid and save it to a file. This is a fast process and generates a skeleton with your model for variable generation. All information should be contained in there. You can check out the file and make sure everything is set up correctly.

2. Now you read the file from the prepare step and draw random variables from the stored distributions. You can save these random variables to a new file. But if you generate a lot of variables, then the file can get big. And often you don't care for the exact variables, but you only want to derive quantities from them, for example the mean. You could calculate the mean, save it and then discard the random variables. Or compress the random variables into a histogram, which is handy for visualization.

To save the generated data in step two, we will benefit from having set up our distributions in a DataArray. We can simply create another DataArray that holds the mean for each distribution. This DataArray has the same dimensions that we defined before. And to combine all information into the same object, we can ceate an `xarray.Dataset` that holds all of our DataArrays.

Configs
============================================================
To automate the process as much as convenient, the prepare and compute steps take config files to define the distributions. These "config files" don't actually follow a config syntax, but rather python. This makes them more flexible and allows to define functions. But you can also steer the behaviour by setting options as simple variables.

:preparedOutputFile: filename for `xarray.Dataset` after prepare step
:computedOutputFile: filename for `xarray.Dataset` after compute step

:smartBins_random: If `True`, then the binning for the histogram of random variables will be chosen dynamically based on the two variables ``smartBins_nBins`` and ``smartBins_nStddev``.

                   If ``False``, then the histogram binning is predefined with the three variables ``randomHistLowX``, ``randomHistHighX``, ``randomHistStepX``.

:smartBins_nBins: Number of bins for random variables
:smartBins_nStddev: Minimum and maximum random value for binning in terms of standard deviation.

:randomHistLowX: Minimum random value in histogram.
:randomHistHighX: Maximum random value in histogram.
:randomHistStepX: Bin width of random value in histogram.

:compute_nRandom: The number of random variables to be drawn for each distribution.

:generationMode: Either ``"random"``, which only generates random values, or ``"randomAndPValues"`` (default), which takes much longer, but also calculates p-values.
:filterActions: Different actions are defined that can be executed on the random variables, for example calculating the mean or stddev. Pass a list of strings selecting which ones you want to calculate. If you don't define filterActions, all actions will be executed.
:randomSeed: This sets the random seed during compute. It is equivalent to ``DistributionBase.setRNG(seed=10)``.

:getDataArray(): This function needs to be written by hand to return the `xarray.DataArray` containing distributions deriving from DistributionBase. For more information, see below.

To give an example for the non-trivial function ``getDataArray()``, the following code creates an array that is very similar to what we've defined manually in :ref:`drawing-random-numbers`. We also add the other options needed to create a working example:

.. literalinclude:: ../faketoys/config/example.py


Run the example
============================================================
Now that we've defined a nice example for a model, we can run it and draw random numbers. Using the file defined above in the directory `config/example.py`, we can execute the commands::

  ./prepare.py config/example.py
  ./compute.py config/example.py

The prepare step should execute very fast and also the compute step should be fairly quick. Be aware, compute can take much longer if you define a larger grid and generate more random numbers. At each step, an output file is written. We can look at either of them with the handy script `fakeopen.py`, which will open an interactive python session. In this session, the variable ds will contain our `xarray.Dataset` and we can look at it with the very useful `xarray` visualization methods::

  ./fakeopen.py computed_example.pkl
  0:00:01 INFO: Adding input filename 'computed_example.pkl' as '_file0'
  0:00:01 INFO: Adding contained dataset as 'ds'
  >>> import matplotlib.pyplot as plt
  >>> _ = ds["mean"].plot()
  >>> plt.show()

This should show you a nice color-coded 2-dimensional histogram of the mean value for each distribution. We can also look at the histogrammed distribution of random variables in one configuration. Let's pick the configuration where the Gaussian has the mean 100 and the Poisson distribution the mean 10::

  >>> from faketoys import plot
  >>> ds.coords
  Coordinates:
  * Gaus_mean  (Gaus_mean) int64 100 110 120
  * Pois_mean  (Pois_mean) int64 10 50 70
  >>> da = ds.loc[{"Gaus_mean": 100, "Pois_mean": 10}].["h_random"]
  >>> _ = plot.histo1d_fromDataArray(da)
  >>> plt.show()

The output of line 2 reminds us which coordinates we defined for our Dataset. We can pick the desired one in the next line and also select the DataArray `h_random` (which was created in `compute.py`). To simplify the plotting, we can pass the DataArray to the function `plot.histo1d_fromDataArray()` and should get a nice distribution of random values.
