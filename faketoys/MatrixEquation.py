from faketoys import EquationBase
from faketoys import FakeFactorEquation
import random
import numpy as np

class MatrixEquation(EquationBase.EquationBase):

    def getEfficiency(self, trueType, measuredType, lepIndex):
        if (trueType == "r" and measuredType == "t"):
            self.message("r[" + str(lepIndex) + "] = " + str(self.r[lepIndex]), 4)
            return self.r[lepIndex]
        elif (trueType == "f" and measuredType == "t"):
            self.message("f[" + str(lepIndex) + "] = " + str(self.f[lepIndex]), 4)
            return self.f[lepIndex]
        elif (trueType == "r" and measuredType == "l"):
            self.message("1-r[" + str(lepIndex) + "] = " + str(1-self.r[lepIndex]), 4)
            return (1. - self.r[lepIndex])
        elif (trueType == "f" and measuredType == "l"):
            self.message("1-f[" + str(lepIndex) + "] = " + str(1-self.f[lepIndex]), 4)
            return (1. - self.f[lepIndex])
        print("error")
        return None

    def getMatrixElement(self, trueTypes, measuredTypes):
        element = 1.
        for lepIndex in range(self.nLeptons):
            element = element * self.getEfficiency(trueTypes[lepIndex], measuredTypes[lepIndex], lepIndex)
        return element

    ### initializing / building equation

    def buildMatrix(self):
        m = np.zeros([self.dimension, self.dimension])
        self.message("building matrix", 2)
        for y in range(self.dimension):
            for x in range(self.dimension):
                trueTypes = self.indexToTrueTypes(x)
                measuredTypes = self.indexToMeasuredTypes(y)
                m[y, x] = self.getMatrixElement(trueTypes, measuredTypes)
                self.message("matrix element " + str(x) + ", " + str(y) + " = " + str(m[y,x]) , 4)
        self.message("matrix built:", 4)
        self.message(str(m), 4)
        return m

    def initializeEfficiencies(self):
        r = []
        f = []
        for i in range(self.nLeptons):
            r.append(random.uniform(0, 1))
            self.message("r[" + str(i) + "] = " + str(r[i]), 2)
            f.append(random.uniform(0, 1))
            self.message("f[" + str(i) + "] = " + str(f[i]), 2)
        self.efficienciesInitialized = True
        return r, f

    def initializeVector(self):
        v = np.empty([self.dimension, 1])
        self.message("building vector", 2)
        for y in range(self.dimension):
            v[y, 0] = random.randrange(100, 10000)
            self.message("vector element " + str(y) + " = " + str(v[y, 0]), 4)
        self.message("vector built", 4)
        self.message(str(v), 4)
        return v

    def initializeRandom(self):
        # initialize = generate random numbers
        # build      = calculate based on existing quantities
        self.r, self.f = self.initializeEfficiencies()
        self.matrix = self.buildMatrix()
        self.matrixInitialized = True
        self.trueVector = self.initializeVector()
        self.trueVectorInitialized = True
        self.measuredVector = self.matrix.dot(self.trueVector)
        self.measuredVectorInitialized = True
        self.message("measuredVector\n" + str(self.measuredVector), 4)
        self.initialized = True

    def __init__(self, nLeptons):
        super().__init__(nLeptons)

        # real and fake efficiencies (one per lepton)
        self.r = []
        self.f = []
        # matrix elements generated from real and fake efficiencies
        self.matrix = None
        # vector on right-hand side with real and fake yields
        self.trueVector = None
        # vector on left-hand side with tight and loose yields
        self.measuredVector = None

        self.initialized               = False
        self.efficienciesInitialized   = False
        self.matrixInitialized         = False
        self.trueVectorInitialized     = False
        self.measuredVectorInitialized = False

        self.debug = -1
        # random.seed(a=0)

    @classmethod
    def fromFakeFactorEquation(cls, fakeFactorEquation):
        # Create an instance of MatrixEquation from an instance of
        # FakeFactorEquation.
        if not isinstance(fakeFactorEquation, FakeFactorEquation.FakeFactorEquation):
            print("ERROR (fromFakeFactorEquation): Pass an instance of FakeFactorEquation to method fromFakeFactorEquation(...). Exiting.")
            sys.exit(1)

        if not fakeFactorEquation.loadDistributions(printWarnings = True):
            print("ERROR (fromFakeFactorEquation): The instance of FakeFactorEquation does not have all necessary values defined to be transformed into a Matrix Equation.")
            return None

        instance = cls(fakeFactorEquation.nLeptons)

        # Convert all member variables from FakeFactorEquation
        # FFs --> fake efficiencies f
        for lepIndex in range(instance.nLeptons):
            FF = fakeFactorEquation.getFakeFactor(lepIndex)
            instance.f.append(instance.fakeFactorToEfficiency(FF.getTrueValue()))

        # Entries of measured vector
        instance.measuredVector = np.empty([instance.dimension, 1])
        for measuredTypesIndex in range(instance.dimension):
            measuredTypes = fakeFactorEquation.indexToMeasuredTypes(measuredTypesIndex)
            nEvents = fakeFactorEquation.getNEvents("", measuredTypes).getTrueValue()
            instance.measuredVector[measuredTypesIndex] = nEvents

        # Converting MC yields into real efficiencies is ambiguous, because the
        # FF method has more inputs than the Matrix Method. For now, the
        # implementation is only done for the one- and two-lepton case.

        nEvents_allRealAllTight = fakeFactorEquation.getNEvents(instance.nLeptons*"r", instance.nLeptons*"t").getTrueValue()
        if nEvents_allRealAllTight == None:
            message(f"ERROR (fromMatrixEquation): The instance of FakeFactorEquation does not have the number of events with one real and tight lepton defined (nLeptons = {instance.nLeptons}). This case could be treated later, but not for now.")
            sys.exit()

        if instance.nLeptons == 1:
            # one lepton
            Ntr = nEvents_allRealAllTight
            Nlr = fakeFactorEquation.getNEvents("r", "l").getTrueValue()
            R = Ntr / Nlr
            # The lepton yield isn't exactly a fake factor, but it is something
            # similar. And the math is the same. So use this method anyway.
            instance.r.append(instance.fakeFactorToEfficiency(R))
        elif instance.nLeptons == 2:
            # two leptons
            Nttrr = nEvents_allRealAllTight
            Ntlrr = fakeFactorEquation.getNEvents("rr", "tl").getTrueValue()
            Nltrr = fakeFactorEquation.getNEvents("rr", "lt").getTrueValue()
            Nllrr = fakeFactorEquation.getNEvents("rr", "ll").getTrueValue()

            r1_1 = instance.fakeFactorToEfficiency(Ntlrr/Nllrr)
            r2_1 = instance.fakeFactorToEfficiency(Nltrr/Nllrr)
            r1_2 = instance.fakeFactorToEfficiency(Nttrr/Nltrr)
            r2_2 = instance.fakeFactorToEfficiency(Nttrr/Ntlrr)

            instance.r.append(instance.compareResultsAndReturnSensibleOne(r1_1, r1_2))
            instance.r.append(instance.compareResultsAndReturnSensibleOne(r2_1, r2_2))
        else:
            message("Trying to convert a FF equation with more than two leptons into a matrix equation. This is not implemented. This will cause an error later.")

        # The efficiencies are calculated, so let's build the matrix
        instance.matrix = instance.buildMatrix()

        # Lastly, calculate the true vector by inverting the matrix
        inverse = np.linalg.inv(instance.matrix)
        instance.trueVector = inverse.dot(instance.measuredVector)
        return instance

    def __str__(self):
        return f"Instance of MatrixEquation with {self.nLeptons} leptons. The variables are initialized to:\n" + self.getStoredValuesAsString()

    def getStoredValuesAsString(self):
        return f"f = {self.f}\nr = {self.r}\ntrueVector = {self.trueVector}\nmeasuredVector = {self.measuredVector}"

    def getNEvents(self, trueTypes, measuredTypes):
        if (not self.checkTrueTypes(trueTypes, allowEmpty = True)) or (not self.checkMeasuredTypes(measuredTypes, allowEmpty = True)):
            print("ERROR: Returning prematurely from getNEvents(...)")
            return None

        if (len(measuredTypes) == 0 and len(trueTypes) == 0):
            # sum over all elements
            res1 = np.ones(self.dimension).dot(self.trueVector)
            res2 = np.ones(self.dimension).dot(self.measuredVector)
            if not self.valuesEqual(res1, res2):
                print("ERROR: Internal consistency check failed. The sum of events in trueVector and measuredVector don't agree. result1 = {res1}, result2 = {res2}.")
                return None
            return res1.item()
        if (len(measuredTypes) == 0):
            index = self.trueTypesToIndex(trueTypes)
            return self.trueVector[index].item()
        if (len(trueTypes) == 0):
            index = self.measuredTypesToIndex(measuredTypes)
            return self.measuredVector[index].item()

        # These were the easy cases. Now we need to do some math.
        # Use the element of trueVector and multiply the correct efficiencies
        index = self.trueTypesToIndex(trueTypes)
        efficiency = self.getMatrixElement(trueTypes, measuredTypes)
        return efficiency * self.trueVector[index].item()

    def getFakeFactor(self, lepIndex):
        return (self.f[lepIndex] / (1 - self.f[lepIndex]))
