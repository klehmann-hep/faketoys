import datetime
startTime = datetime.datetime.now()

import sys
import faketoys.stats.dist_base
import faketoys.stats.derived_dist_base
import faketoys.stats.dists
import faketoys.stats.derived_dists
import faketoys.variations.models
import faketoys.variations.model_base
import faketoys.variations.config

# Map old module names to the new modules. This allows to read old
# pickled files.

sys.modules['faketoys.stats.DistributionBase'] = faketoys.stats.dist_base
sys.modules['faketoys.stats.EvaluationMode'] = faketoys.stats.dist_base
sys.modules['faketoys.stats.DerivedDistributionBase'] = faketoys.stats.derived_dist_base
sys.modules['faketoys.stats.Gaussian'] = faketoys.stats.dists
sys.modules['faketoys.stats.Poisson'] = faketoys.stats.dists
sys.modules['faketoys.stats.Delta'] = faketoys.stats.dists
sys.modules['faketoys.stats.Dummy'] = faketoys.stats.dists
sys.modules['faketoys.stats.FakeFactor'] = faketoys.stats.derived_dists
sys.modules['faketoys.stats.SumOfDistributions'] = faketoys.stats.derived_dists
sys.modules['faketoys.stats.ProductOfDistributions'] = faketoys.stats.derived_dists
sys.modules['faketoys.variations.AbsoluteVariation'] = faketoys.variations.models
sys.modules['faketoys.variations.SymmetricLinearVariation'] = faketoys.variations.models
sys.modules['faketoys.variations.VariationModelBase'] = faketoys.variations.model_base
sys.modules['faketoys.variations.VariationConfig'] = faketoys.variations.config
