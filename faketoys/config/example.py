import xarray
from faketoys import utils
from faketoys.stats import dists, derived_dists

def getDataArray():
    # Define coordinates and create xarray.DataArray
    bins = {
        "Gaus_mean": [100, 110, 120],
        "Pois_mean": [10, 50, 70],
        }
    dimensions = [x for x in bins.keys()]
    array = xarray.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")

    # Initialize every entry in the DataArray with the method
    # initializeValue. The initialized array needs to be passed.
    # All additional arguments will be forwarded to initializeValue.
    utils.initializeArray(initializeValue, array, gaus_stddev=20)
    return array

def initializeValue(coordinates, index, gaus_stddev):
    # Based on the coordinates from the DataArray, we can initialize
    # the sum of Gaussian and Poisson distribution here. The standard
    # deviation of the Gaussian is the same in all cases.
    gaus_mean = coordinates["Gaus_mean"]
    pois_mean = coordinates["Pois_mean"]
    g = dists.Gaussian(gaus_mean, gaus_stddev)
    p = dists.Poisson(pois_mean)
    s = derived_dists.SumOfDistributions([g, p], name=f"G{gaus_mean}_P{pois_mean}")
    return s

preparedOutputFile = f"prepared_example.pkl"
computedOutputFile = f"computed_example.pkl"
smartBins_random = True
smartBins_nBins = 100
smartBins_nStddev = 4

compute_nRandom = int(1e3)
randomSeed = 10
filterActions = ["random_binning", "h_random", "mean", "asymStddev"]
