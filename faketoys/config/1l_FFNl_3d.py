from faketoys import log, utils
import xarray as xr

preparedOutputFile = "prepared_1l_FFNl_3d.pkl"
computedOutputFile = "computed_1l_FFNl_3d.pkl"

smartBins_random = False
randomHistLowX = -10
randomHistHighX = 30
randomHistStepX = 0.5
pValueHistStepX = 0.02

compute_nRandom = int(1e6)

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [125]#, 150, 175]
    bins["FF_Ntr"]     = [75]#, 100, 125]
    bins["FF_Ntr_unc"] = [10]
    bins["FF_Nl"]      = [50, 100, 150, 200, 250, 300, 400, 500, 600, 800]
    bins["FF_Nlr"]     = [25, 50, 75, 100]
    bins["FF_Nlr_unc"] = [2, 4, 7, 10, 15, 20, 25]

    bins["Nl"]         = [9]
    bins["Nlr"]        = [1]
    bins["Nlr_unc"]    = [0]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array, denominatorGreaterThan=20)

    return array
