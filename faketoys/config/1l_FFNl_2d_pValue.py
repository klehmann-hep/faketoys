from faketoys import log, utils
import xarray as xr

preparedOutputFile = "prepared_1l_FFNl_2d_pValue.pkl"
computedOutputFile = "computed_1l_FFNl_2d_pValue.pkl"

smartBins_random = True
smartBins_nBins = 100
smartBins_nStddev = 4

pValueHistStepX = 0.02

compute_nRandom = int(1e6)

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [400]
    bins["FF_Ntr"]     = [0]
    bins["FF_Ntr_unc"] = [5]
    bins["FF_Nl"]      = [500, 1000, 2000, 3000, 5000, 10000, 20000, 50000]
    bins["FF_Nlr"]     = [0]
    bins["FF_Nlr_unc"] = [5, 40, 80, 120, 200, 400]

    bins["Nl"]         = [1000]
    bins["Nlr"]        = [0]
    bins["Nlr_unc"]    = [40]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array, denominatorGreaterThan=20)

    return array
