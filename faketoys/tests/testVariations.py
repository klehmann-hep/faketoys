import unittest

from faketoys.stats import dists, dist_base
from faketoys.variations.config import VariationConfig
from faketoys.variations.models import SymmetricLinearVariation, AbsoluteVariation

class TestVariations(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_variationMatching(self):
        # g1 depends on g2, which depends on g3.
        g1 = dists.Gaussian(0, 2)
        g2 = dists.Gaussian(0, 2)
        g3 = dists.Gaussian(0, 2)

        var50 = SymmetricLinearVariation(0.5)
        varConf_g3 = VariationConfig(g3, var50)
        varConf_g2 = VariationConfig(g2, var50)

        g2.addVariationConfig(varConf_g3)
        g1.addVariationConfig(varConf_g2)

        self.assertTrue(g1.getAffectedVariationConfigs([g2]), varConf_g2)
        self.assertTrue(g2.getAffectedVariationConfigs([g3]), varConf_g3)
        self.assertTrue(g1.getAffectedVariationConfigs([g3]), varConf_g2)
        return

    def test_checkConsistency(self):
        g1 = dists.Gaussian(0, 2)
        g2 = dists.Gaussian(2, 1)
        variationModels = []
        variationModels.append(SymmetricLinearVariation(1))
        variationModels.append(AbsoluteVariation(-3))
        for variationModel in variationModels:
            variationConfig = VariationConfig(g1, variationModel)
            self.assertTrue(variationConfig.checkConsistency())
            variationConfig = VariationConfig(g2, variationModel)
            self.assertFalse(variationConfig.checkConsistency())
        return



if __name__ == '__main__':
    unittest.main()
