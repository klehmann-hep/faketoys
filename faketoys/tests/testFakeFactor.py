import unittest

import numpy as np
from faketoys.stats import derived_dist_base, dist_base, dists, derived_dists
from faketoys.variations.config import VariationConfig
from faketoys.variations.models import AbsoluteVariation, SymmetricLinearVariation

class TestFakeFactor(unittest.TestCase):

    def setUp(self):
        Nt  = dists.Poisson(16, name = "Nt")
        Ntr = dists.Gaussian(13, 1, name = "Ntr")
        Nl  = dists.Poisson(36, name = "Nl")
        Nlr = dists.Gaussian(6, 3, name = "Nlr")
        self.FF = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl, Nlr = Nlr, name = "FF"
                                            )

        Nt  = dists.Poisson(16, name = "Nt_2")
        Ntr = dists.Gaussian(13, 1, name = "Ntr_2")
        Nl  = dists.Poisson(36, name = "Nl_2")
        Nlr = dists.Gaussian(6, 3, name = "Nlr_2")
        self.sys1 = dists.Gaussian(0, 1, name = "sys1")
        varConf1 = VariationConfig(self.sys1, AbsoluteVariation(1.5))
        self.sys2 = dists.Gaussian(0, 1, name = "sys2")
        varConf2 = VariationConfig(self.sys2, SymmetricLinearVariation(0.5))
        Nt.addVariationConfig(varConf1)
        Nl.addVariationConfig(varConf2)
        self.FF_sys = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl, Nlr = Nlr, name = "FF_sys"
                                                )

        Nt = dists.Delta(16)
        Ntr = dists.Delta(15)
        Nl  = dists.Poisson(2, name = "Nl_new")
        Nlr = dists.Gaussian(1.5, 0.5, name = "Nlr_new")
        self.FF_smallDen = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl, Nlr = Nlr, name = "FF small denominator", denominatorGreaterThan=0.1)
        return

    def tearDown(self):
        return

    def test_basics(self):
        self.assertEqual(self.FF.getTrueValue(), 0.1)
        self.assertEqual(len(self.FF.getExpression().error_components()), 4)
        self.assertEqual(len(self.FF.getExpressionStatOnly().error_components()), 4)
        self.assertEqual(len(self.FF.findAllSources()), 4)

        self.assertEqual(self.FF_sys.getTrueValue(), 0.1)
        self.assertEqual(len(self.FF_sys.getExpression().error_components()), 6)
        self.assertEqual(len(self.FF_sys.getExpressionStatOnly().error_components()), 4)

        self.assertTrue(isinstance(self.FF.__repr__(), str))

        # This compares all elements ignoring the order
        self.assertCountEqual(self.FF_sys.findAllSources(), [self.FF_sys.Nt, self.FF_sys.Ntr, self.FF_sys.Nl, self.FF_sys.Nlr, self.sys1, self.sys2])

        return

    def test_variations(self):
        variedValues     = self.FF.getListOfVariedValues()
        variedValues_sys = self.FF_sys.getListOfVariedValues()
        self.assertCountEqual(variedValues, variedValues_sys)

        variedValues_sys = self.FF_sys.getListOfVariedValues(systSources = [self.sys1])
        self.assertEqual(len(variedValues_sys), 2*5)

        variedValues_sys = self.FF_sys.getDictOfVariedValues(systSources = [self.sys1, self.sys2], statUnc = False)
        # [ ( (16 + 1.5) - 13 ) / (36 - 6) , ( (16 - 1.5) - 13 ) / (36 - 6) ]
        self.assertEqual(variedValues_sys[self.sys1], [0.15, 0.05])
        # [ (16 - 13) / (36 * 1.5 - 6) ,  (16 - 13) / (36 * 0.5 - 6) ]
        self.assertEqual(variedValues_sys[self.sys2], [0.0625, 0.25])

        return

    def test_getRandom(self):
        value     = self.FF.getRandom(statUnc = False)
        value_sys = self.FF_sys.getRandom(statUnc = False)
        self.assertEqual(value, value_sys)
        self.assertEqual(value, 0.1)

        value = self.FF.getRandom(statUnc = True, cacheIndex = 1)
        sameValue = self.FF.getRandom(statUnc = True, cacheIndex = 1)
        self.assertEqual(value, sameValue)

        # This value should be different, because it does not use the same
        # underlying distributions
        diffValue = self.FF_sys.getRandom(statUnc = True, cacheIndex = 1)
        self.assertNotEqual(value, diffValue)

        # This should return None, because DistributionBase.getRandom() doesn't
        # like when nRandom is used with other arguments.
        self.assertEqual(self.FF.getRandom(nRandom=2, cacheIndex=1), None)

        randVars = self.FF.getRandom(nRandom = 3)
        self.assertEqual(len(randVars), 3)

        return

    def test_smallDenominatorProtection(self):
        dictOfValues = {}
        dictOfValues["Nt"] = 1
        dictOfValues["Ntr"] = 0
        dictOfValues["Nl"] = 1
        dictOfValues["Nlr"] = 0.5

        self.assertAlmostEqual(self.FF_smallDen.calculateValue(dictOfValues), 1 / 0.5)
        dictOfValues["Nl"] = 0.55
        self.assertAlmostEqual(self.FF_smallDen.calculateValue(dictOfValues), 1 / 0.1)
        self.assertAlmostEqual(self.FF.calculateValue(dictOfValues), 1 / 0.05)
        dictOfValues["Nl"] = 0.4
        self.assertAlmostEqual(self.FF_smallDen.calculateValue(dictOfValues), 1 / 0.1)
        self.assertAlmostEqual(self.FF.calculateValue(dictOfValues), 1 / -0.1)

        for i in range(100):
            random = self.FF_smallDen.getRandom()
            self.assertTrue(random <= 10)
            self.assertTrue(random > 0)
        for random in self.FF_smallDen.getRandom(nRandom=100):
            self.assertTrue(random <= 10)
            self.assertTrue(random > 0)
        return


if __name__ == '__main__':
    unittest.main()
